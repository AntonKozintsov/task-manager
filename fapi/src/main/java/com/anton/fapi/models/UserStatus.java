package com.anton.fapi.models;

import lombok.Data;

@Data
public class UserStatus {
    private Long userId;
    private String status;
}

