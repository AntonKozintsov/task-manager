package com.anton.fapi.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class AttachmentModel implements Serializable {

    private String imagecontent;
}
