package com.anton.fapi.models;

import lombok.Data;

import java.util.List;

@Data
public class PageableTask {
    private List<TaskModel> taskModelList;
    private Long totalTasks;
}
