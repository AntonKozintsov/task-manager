package com.anton.fapi.models;

import lombok.Data;

@Data
public class Priority {
    private Long id;
    private String priority;

}
