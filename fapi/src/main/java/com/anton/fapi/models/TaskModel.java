package com.anton.fapi.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TaskModel {
    private Long id;
    private String priority;
    private String status;
    private String taskIdentity;
    private String description;
    private String projectCode;
    private String created;
    private String updated;
    private String resolved;
    private String closed;
    private String dueDate;
    private String estimation;
    private String assignee;
    private String reporter;
    private List<String> attachments;

}
