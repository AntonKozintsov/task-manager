package com.anton.fapi.models;

import lombok.Data;

@Data
public class StatusModel {

    private String status;
}
