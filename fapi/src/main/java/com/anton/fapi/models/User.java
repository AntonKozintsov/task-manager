package com.anton.fapi.models;

import lombok.Data;

import java.util.List;

@Data
public class User {

    private Long id;
    private String name;
    private String username;
    private String email;
    private String password;
    private List<String> roles;
}
