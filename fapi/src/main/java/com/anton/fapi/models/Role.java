package com.anton.fapi.models;

import lombok.Data;

@Data
public class Role {
    private Long id;
    private String role;
}
