package com.anton.fapi.util;

import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class DateUtil {

    public String dateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String stringDate;
        if (date == null) {
            stringDate = "Not updated yet";
        } else {
            stringDate = sdf.format(date);
        }
        return stringDate;
    }

    public Date stringToDate(String string) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = null;
        if (string == null) {
            return null;
        }
        try {
            date = sdf.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public String JsonDateToStringDate(String string) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = null;
        try {
            date = sdf.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return addHoursToJavaUtilDate(output.format(date));
    }

    public String addHoursToJavaUtilDate(String date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(stringToDate(date));
        calendar.add(Calendar.HOUR_OF_DAY, 3);
        return dateToString(calendar.getTime());
    }
}
