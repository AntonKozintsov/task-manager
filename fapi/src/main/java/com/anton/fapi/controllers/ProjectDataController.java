package com.anton.fapi.controllers;

import com.anton.fapi.models.ProjectModel;
import com.anton.fapi.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectDataController {

    private final ProjectService projectService;

    @Autowired
    public ProjectDataController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("")
    public ResponseEntity<List<ProjectModel>> getAllProjects() {
        return ResponseEntity.ok(projectService.findAll());
    }

    @GetMapping("/{projectCode}")
    public ResponseEntity<List<ProjectModel>> getAutocompleteProjects(@PathVariable String projectCode) {
        return ResponseEntity.ok(projectService.findAutocomplete(projectCode));
    }

    @PostMapping("")
    public ResponseEntity<ProjectModel> saveTask(@RequestBody ProjectModel projectModel) {
        System.out.println(projectModel);
        if (projectModel != null) {
            return ResponseEntity.ok(projectService.saveProject(projectModel));
        }
        return null;
    }
}
