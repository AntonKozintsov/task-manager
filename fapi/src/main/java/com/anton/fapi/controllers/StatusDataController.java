package com.anton.fapi.controllers;

import com.anton.fapi.models.StatusModel;
import com.anton.fapi.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/statuses")
public class StatusDataController {

    private final StatusService statusService;

    @Autowired
    public StatusDataController(StatusService statusService) {
        this.statusService = statusService;
    }

    @GetMapping("")
    public ResponseEntity<List<StatusModel>> getAllProjects() {
        return ResponseEntity.ok(statusService.findAll());
    }
}
