package com.anton.fapi.controllers;

import com.anton.fapi.models.AttachmentModel;
import com.anton.fapi.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/tasks/attachments")
public class AttachmentDataController {

    private final AttachmentService attachmentService;

    @Autowired
    public AttachmentDataController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @PostMapping("")
    public ResponseEntity<String> uploadAttachment(@RequestParam Long taskId, @RequestParam(value = "file", required = false) MultipartFile file) {
        return attachmentService.uploadFile(taskId, file);
    }

    @GetMapping(value = "/{filename}")
    public AttachmentModel getFileFromFileSystem(@PathVariable String filename) throws IOException {
        return attachmentService.downloadFile(filename);
    }
}
