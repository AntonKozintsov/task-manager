package com.anton.fapi.controllers;

import com.anton.fapi.models.PageableTask;
import com.anton.fapi.models.TaskModel;
import com.anton.fapi.models.UserStatus;
import com.anton.fapi.service.TaskService;
import com.anton.fapi.service.UserStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskDataController {

    private final TaskService taskService;

    private final UserStatusService userStatusService;

    @Autowired
    public TaskDataController(TaskService taskService, UserStatusService userStatusService) {
        this.taskService = taskService;
        this.userStatusService = userStatusService;
    }

    @GetMapping("")
    public ResponseEntity<List<TaskModel>> getAllTasks() {
        return ResponseEntity.ok(taskService.findAll());
    }

    @GetMapping("/pageable/{pageNumber}/{currentTasksOnPage}")
    public ResponseEntity<PageableTask> getPageableTasks(@PathVariable Integer pageNumber, @PathVariable Integer currentTasksOnPage) {
        return ResponseEntity.ok(taskService.findAll(pageNumber, currentTasksOnPage));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskModel> getTask(@PathVariable Long id) {
        return ResponseEntity.ok(taskService.getTaskById(id));
    }

    @PreAuthorize("hasRole('PM')")
    @PostMapping("/save")
    public ResponseEntity<TaskModel> saveTask(@RequestBody TaskModel task) {
        if (task != null) {
            return ResponseEntity.ok(taskService.addTask(task));
        }
        return null;
    }

    @PreAuthorize("hasRole('DEVELOPER') || ('TESTER')")
    @PostMapping("")
    public ResponseEntity<TaskModel> updateStatus(@RequestBody UserStatus userStatus) {
        if (userStatus != null) {
            return ResponseEntity.ok(userStatusService.update(userStatus));
        }
        return null;
    }

    @GetMapping("/search/{taskIdentity}")
    public ResponseEntity<List<TaskModel>> getAutocompleteTasks(@PathVariable String taskIdentity) {
        return ResponseEntity.ok(taskService.findAutocomplete(taskIdentity));
    }
//    @PostMapping("/assign")
//    public ResponseEntity<TaskModel> toAssign(@RequestBody TaskModel task) {
//        if (task!=null) {
//
//        }
//    }
}
