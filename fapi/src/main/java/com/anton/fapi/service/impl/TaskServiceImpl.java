package com.anton.fapi.service.impl;

import com.anton.fapi.models.PageableTask;
import com.anton.fapi.models.TaskModel;
import com.anton.fapi.service.TaskService;
import com.anton.fapi.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private final DateUtil dateUtil;
    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Autowired
    public TaskServiceImpl(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    @Override
    public PageableTask findAll(Integer pageNumber, Integer currentTasksOnPage) {
        RestTemplate restTemplate = new RestTemplate();
        PageableTask taskModelResponse = restTemplate.getForObject(backendServerUrl + "/api/tasks/pageable/" + pageNumber + "/" + currentTasksOnPage, PageableTask.class);
        return taskModelResponse == null ? new PageableTask() : taskModelResponse;
    }

    @Override
    public List<TaskModel> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        TaskModel[] taskModelResponse = restTemplate.getForObject(backendServerUrl + "/api/tasks", TaskModel[].class);
        if (taskModelResponse == null) return Collections.emptyList();
        else return Arrays.asList(taskModelResponse);
    }

    @Override
    public TaskModel addTask(TaskModel task) {
        RestTemplate restTemplate = new RestTemplate();
//        if (task.getReporter()==null) {
//            task.setReporter("projMax");
//        }
        task.setStatus("OPENED");
        task.setCreated(dateUtil.dateToString(new Date()));
        task.setUpdated(dateUtil.dateToString(new Date()));
        if (task.getEstimation() != null) {
            task.setEstimation(dateUtil.JsonDateToStringDate(task.getEstimation()));
        }
        if (task.getDueDate() != null) {
            task.setDueDate(dateUtil.JsonDateToStringDate(task.getDueDate()));
        }
        return restTemplate.postForEntity(backendServerUrl + "/api/tasks/addTask", task, TaskModel.class).getBody();
    }

    @Override
    public TaskModel updateTask(TaskModel task) {
        RestTemplate restTemplate = new RestTemplate();
        task.setUpdated(dateUtil.JsonDateToStringDate(task.getUpdated()));
        return restTemplate.postForEntity(backendServerUrl + "/api/tasks/addTask", task, TaskModel.class).getBody();
    }

    @Override
    public TaskModel getTaskById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        TaskModel taskModel = restTemplate.getForObject(backendServerUrl + "/api/tasks/" + id, TaskModel.class);
        System.out.println(taskModel);
        return taskModel;
    }

    @Override
    public List<TaskModel> findAutocomplete(String taskIdentity) {
        RestTemplate restTemplate = new RestTemplate();
        TaskModel[] taskModelResponse = restTemplate.getForObject(backendServerUrl + "/api/tasks/search/" + taskIdentity, TaskModel[].class);
        return taskModelResponse == null ? Collections.emptyList() : Arrays.asList(taskModelResponse);
    }

}
