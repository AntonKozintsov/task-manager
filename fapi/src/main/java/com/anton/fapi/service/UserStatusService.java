package com.anton.fapi.service;

import com.anton.fapi.models.TaskModel;
import com.anton.fapi.models.UserStatus;

public interface UserStatusService {

    TaskModel update(UserStatus userStatus);

}
