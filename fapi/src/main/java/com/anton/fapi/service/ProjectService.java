package com.anton.fapi.service;

import com.anton.fapi.models.ProjectModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProjectService {

    @Transactional
    List<ProjectModel> findAll();


    List<ProjectModel> findAutocomplete(String projectCode);

    ProjectModel saveProject(ProjectModel projectModel);
}
