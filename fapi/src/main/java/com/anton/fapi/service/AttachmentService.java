package com.anton.fapi.service;

import com.anton.fapi.models.AttachmentModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;


public interface AttachmentService {

    ResponseEntity<String> uploadFile(Long taskId, MultipartFile file);

    AttachmentModel downloadFile(String fileName);
}
