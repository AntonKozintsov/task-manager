package com.anton.fapi.service;

import com.anton.fapi.models.StatusModel;

import java.util.List;

public interface StatusService {

    List<StatusModel> findAll();
}
