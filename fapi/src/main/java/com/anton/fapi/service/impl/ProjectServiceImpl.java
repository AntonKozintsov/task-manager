package com.anton.fapi.service.impl;

import com.anton.fapi.models.ProjectModel;
import com.anton.fapi.service.ProjectService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Value("${backend.server.url}")
    private String backendServerUrl;


    @Override
    public List<ProjectModel> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        ProjectModel[] projectModelResponse = restTemplate.getForObject(backendServerUrl + "/api/projects", ProjectModel[].class);
        return projectModelResponse == null ? Collections.emptyList() : Arrays.asList(projectModelResponse);
    }

    @Override
    public List<ProjectModel> findAutocomplete(String projectCode) {
        RestTemplate restTemplate = new RestTemplate();
        ProjectModel[] projectModelResponse = restTemplate.getForObject(backendServerUrl + "/api/projects/" + projectCode, ProjectModel[].class);
        return projectModelResponse == null ? Collections.emptyList() : Arrays.asList(projectModelResponse);
    }

    @Override
    public ProjectModel saveProject(ProjectModel projectModel) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl + "/api/projects", projectModel, ProjectModel.class).getBody();
    }
}
