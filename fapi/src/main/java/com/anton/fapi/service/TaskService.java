package com.anton.fapi.service;

import com.anton.fapi.models.PageableTask;
import com.anton.fapi.models.TaskModel;

import java.util.List;

public interface TaskService {


    PageableTask findAll(Integer pageNumber, Integer currentTasksOnPage);

    List<TaskModel> findAll();

    TaskModel addTask(TaskModel task);


    TaskModel updateTask(TaskModel task);

    TaskModel getTaskById(Long id);

    List<TaskModel> findAutocomplete(String taskIdentity);
}
