package com.anton.fapi.service.impl;

import com.anton.fapi.models.StatusModel;
import com.anton.fapi.service.StatusService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public List<StatusModel> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        StatusModel[] statusModelResponse = restTemplate.getForObject(backendServerUrl + "/api/statuses", StatusModel[].class);
        if (statusModelResponse == null) return Collections.emptyList();
        else return Arrays.asList(statusModelResponse);
    }
}
