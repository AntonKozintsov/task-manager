package com.anton.fapi.service.impl;

import com.anton.fapi.models.AttachmentModel;
import com.anton.fapi.service.AttachmentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public ResponseEntity<String> uploadFile(Long taskId, MultipartFile file) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        // This nested HttpEntiy is important to create the correct
        // Content-Disposition entry with metadata "name" and "filename"
        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        ContentDisposition contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename(file.getOriginalFilename())
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        HttpEntity<byte[]> fileEntity = null;
        try {
            fileEntity = new HttpEntity<>(file.getBytes(), fileMap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileEntity);
        body.add("taskId", taskId);

        HttpEntity<MultiValueMap<String, Object>> requestEntity =
                new HttpEntity<>(body, headers);
        return restTemplate.exchange(backendServerUrl + "/api/tasks/attachments", HttpMethod.POST, requestEntity, String.class);
    }

    @Override
    public AttachmentModel downloadFile(String fileName) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(backendServerUrl + "/api/tasks/attachments/" + fileName, AttachmentModel.class);
    }
}
