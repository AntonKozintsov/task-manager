package com.anton.fapi.service.impl;

import com.anton.fapi.models.TaskModel;
import com.anton.fapi.models.UserStatus;
import com.anton.fapi.service.UserStatusService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserStatusServiceImpl implements UserStatusService {

    @Value("${backend.server.url}")
    private String backendServerUrl;

    @Override
    public TaskModel update(UserStatus userStatus) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForEntity(backendServerUrl + "/api/tasks", userStatus, TaskModel.class).getBody();
    }

}
