package com.anton.fapi.service;

import com.anton.fapi.models.User;

import java.util.List;

public interface UserService {


    User findByUsername(String username);

    List<User> findAll();

    User save(User user);
}
