import { Component, OnInit } from '@angular/core';
import {TableComponent} from "../table/table.component";
import {TaskService} from "../../services/task.service";
import {TaskSharingService} from "../../services/task-sharing.service";
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {TokenStorageService} from "../../services/toke-storage.service";

@Component({
  selector: 'app-project-navbar',
  templateUrl: './project-navbar.component.html',
  styleUrls: ['./project-navbar.component.styl']
})
export class ProjectNavbarComponent extends TableComponent implements OnInit {

  taskIdentityControl : FormControl = new FormControl();

  constructor(taskService: TaskService, data: TaskSharingService, router: Router, tokenService: TokenStorageService) {
    super(taskService, data, tokenService, router);

  }

  ngOnInit() {
  }

}
