import {Component, OnInit, TemplateRef} from '@angular/core';
import {TaskViewComponent} from "../task-view/task-view.component";
import {Subscription} from "rxjs";
import {Task} from "../models/task";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {TaskService} from "../../services/task.service";

@Component({
  selector: 'app-task-navbar',
  templateUrl: './task-navbar.component.html',
  styleUrls: ['./task-navbar.component.styl']
})
export class TaskNavbarComponent  implements OnInit {
  public editMode = false;
  private subscriptions: Subscription[] = [];
  public modalRef: BsModalRef;
  private task: Task = new Task;

    constructor(private comp: TaskViewComponent ,
                 private modalService: BsModalService) {
  }

  public modifyTask(): void {
      console.log(this.comp.task);

    this.subscriptions.push(this.comp.taskService.addTask(this.comp.task).subscribe(() => {
      this.closeModalWindow();
    }));
  }


  public editTask() {
    this.comp.editTask();
  }
  public updateStatus(status: string) {
    this.comp.updateStatus(status);
  }

  public closeModalWindow(): void {
    this.modalRef.hide();
  }

  public openModalWindow(template: TemplateRef<any>, task: Task): void {
    if (task) {
      this.editMode = true;
      this.task = Task.cloneBase(this.comp.task);
    } else {
      this.editMode = false;
    }
    this.modalRef = this.modalService.show(template);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngOnInit() {
      console.log(this.comp.task);
  }


}
