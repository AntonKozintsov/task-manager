import {Component, OnInit} from '@angular/core';
import {TableComponent} from "../table/table.component";
import {Task} from "../models/task";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.styl']
})
export class PaginationComponent extends TableComponent implements OnInit {

  public currentPage: number;
  public tasksAmount;
  public tasksOnPage: string [] = ['5', '8', '10'];

  ngOnInit() {
    this.data.currentPage.subscribe(currentPage => {
      this.currentPage = currentPage;
    });
    this.data.currentTasksOnPage.subscribe(currentTasksOnPage => {
      this.currentTasksOnPage = currentTasksOnPage;
      this.data.currentTaskAmount.subscribe(tasksAmount => {
        this.tasksAmount = tasksAmount;
        this.totalPagesArr = Array(Math.ceil(this.tasksAmount / this.currentTasksOnPage)).fill(0).map((x, i) => i)
      });
    });

  }

  public loadPageableTasks(page: number): void {
    if (this.totalPagesArr.length > page && page > 0) {
      this.currentPage = page;
    } else if (page < 0) {
      this.currentPage = this.totalPagesArr.length - 1;
      if (this.currentPage == -1) {
        this.currentPage = 0;
      }

    } else {
      this.currentPage = 0;
    }
    this.data.updateCurrentPage(this.currentPage);
  }

  public updateCurrentTasksOnPage(tasksOnPage: number) {
    this.data.updateCurrentTasksOnPage(tasksOnPage);
    if (this.tasksAmount < this.currentTasksOnPage) {
      this.totalPagesArr = [0];
    } else {
      this.totalPagesArr = Array(Math.ceil(this.tasksAmount / this.currentTasksOnPage)).fill(0).map((x, i) => i)
    }
    this.data.updateCurrentPage(0);
  }


}
