import {Component, OnInit} from '@angular/core';
import {Task} from "../models/task";
import {Subscription} from "rxjs";
import {TaskService} from "../../services/task.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserStatus} from "../models/userStatus";
import {FormControl, FormGroup} from "@angular/forms";
import {el} from "@angular/platform-browser/testing/src/browser_util";
import {StatusService} from "../../services/status.service";
import {Status} from "../models/status";
import {Attachment} from "../models/attachment";
import {AttachmentService} from "../../services/attachment.service";
import {DomSanitizer} from "@angular/platform-browser";
import {AttachmentModel} from "../models/attachment-model";

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.styl']
})
export class TaskViewComponent implements OnInit {

  public task: Task = new Task();
  public attachment: Attachment = new Attachment();
  public isDisabled: boolean;
  public subscriptions: Subscription[] = [];
  public id =  this.activateRoute.snapshot.params['userId'];
  public status: UserStatus = new UserStatus();
  public statuses: Status[];
  taskForm: FormGroup;
  public selectedFiles: FileList;
  public currentFile: File;
  public formData: FormData;
  public attachmentDisabled: boolean = true;
  public image: any;
  private readonly imageType: string = 'data:image/jpg;base64';

  constructor(public taskService: TaskService, private statusService: StatusService, private attachmentService: AttachmentService,
              private activateRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.loadTask();

    this.loadStatuses();
    this.taskForm = new FormGroup({
      descriptionForm: new FormControl(),
      priorityForm: new FormControl(),
      statusForm: new FormControl(),
      assigneeForm: new FormControl(),
      reporterForm: new FormControl(),
      estimationForm: new FormControl(),
      createdForm: new FormControl(),
      updatedForm: new FormControl(),
      resolvedForm: new FormControl(),
      closedForm: new FormControl()
    });
    this.isDisabled = true;
    this.taskForm.disable();

  }
  public loadTask(): void {
    if (!this.id) {
      this.router.navigate(['/error'], {});
    } else {
      this.getTask();
          // this.loadingService.hide();
    }
  }

  public editTask() {
    // this.loadStatuses();
    if (this.isDisabled==true) {
      this.isDisabled=false;
      this.taskForm.enable();
    } else{
      this.isDisabled=true;
      this.taskForm.disable();
    }
  }

  public updateStatus(status: string): void {
    this.attachmentDisabled = false;
    this.status.status = status;
    this.status.userId = this.id;
    console.log(this.status.userId);
    console.log(this.status);
    this.subscriptions.push(this.taskService.updateStatus(this.status).subscribe(() => {
      this.loadTask();
    }));
  }
  private loadStatuses(): void {
    this.subscriptions.push(this.statusService.getStatuses().subscribe(statuses => {
      this.statuses = statuses as Status[];
    }));
  }

  private selectFile(event) {
      this.selectedFiles = event.target.files;
      console.log(this.selectedFiles);
  }
  private uploadFile() {
    this.formData = new FormData();
    this.formData.append('taskId', this.id);
    this.formData.append('file', this.selectedFiles.item(0), this.selectedFiles.item(0).name );
    this.subscriptions.push(this.attachmentService.loadFile(this.formData).subscribe( () => {
      this.getTask();
      this.formData.delete('taskId');
      this.formData.delete('file');
    } ));
    this.selectedFiles = undefined;

  }
  private getTask() {
    this.subscriptions.push(this.taskService.getTaskById(this.id).subscribe(task => {
      this.task = task;
      console.log(this.task.resolved);
    }));
  }

  getImage(filename: string) {
    this.attachmentService.getImage(filename).subscribe((data :AttachmentModel) => {
      this.image = data.imagecontent;
    })
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

}
