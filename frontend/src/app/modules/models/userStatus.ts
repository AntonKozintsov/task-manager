export class UserStatus {
  userId: string;
  status: string;


  static cloneBase(status: UserStatus): UserStatus {
    const cloned: UserStatus = new UserStatus();
    cloned.userId = status.userId;
    cloned.status = status.status;
    return cloned;
  }
}
