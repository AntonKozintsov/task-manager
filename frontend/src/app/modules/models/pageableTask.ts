import {Task} from "./task";

export class PageableTask {
  private _taskModelList: Task [];
  private _totalTasks: number;

  get taskModelList(): Task[] {
    return this._taskModelList;
  }

  set taskModelList(value: Task[]) {
    this._taskModelList = value;
  }

  get totalTasks(): number {
    return this._totalTasks;
  }

  set totalTasks(value: number) {
    this._totalTasks = value;
  }
}
