export class User {
  id: string;
  login: string;
  name: string;
  password: string;

  static cloneBase(user: User): User {
    const cloned: User = new User();
    cloned.id = user.id;
    cloned.login = user.login;
    cloned.name = user.name;
    cloned.password = user.password;
    return cloned;
  }
}
