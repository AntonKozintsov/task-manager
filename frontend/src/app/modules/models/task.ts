export class Task {
  id: string;
  priority: string;
  status: string;
  taskIdentity: string;
  description: string;
  projectCode: string;
  created: string;
  updated: string;
  resolved:string;
  closed:string;
  dueDate:string;
  estimation:string;
  assignee:string;
  reporter:string;
  attachments: string [];


  static cloneBase(task: Task): Task {
    const clonedTask: Task = new Task();
    clonedTask.id = task.id;
    clonedTask.projectCode = task.projectCode;
    clonedTask.priority = task.priority;
    clonedTask.status = task.status;
    clonedTask.taskIdentity = task.taskIdentity;
    clonedTask.description = task.description;
    clonedTask.projectCode = task.projectCode;
    clonedTask.created = task.created;
    clonedTask.updated = task.updated;
    clonedTask.resolved = task.resolved;
    clonedTask.closed = task.closed;
    clonedTask.dueDate = task.dueDate;
    clonedTask.estimation = task.estimation;
    clonedTask.assignee = task.assignee;
    clonedTask.reporter = task.reporter;
    return clonedTask;
  }
}
