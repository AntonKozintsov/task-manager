export class Project {
   projectCode: string;
   summary: string;

   static cloneBase(project: Project): Project {
     const clonedProject: Project = new Project();
     clonedProject.projectCode = project.projectCode;
     clonedProject.summary = project.summary;
     return clonedProject;
   }
}
