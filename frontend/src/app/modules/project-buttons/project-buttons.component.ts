import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Task} from "../models/task";
import {TaskService} from "../../services/task.service";
import {Subscription} from "rxjs";
import {Project} from "../models/project";
import {ProjectService} from "../../services/project.service";
import {StatusService} from "../../services/status.service";
import {AbstractControl, FormControl, ValidatorFn, Validators} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {TaskSharingService} from "../../services/task-sharing.service";

export function forbiddenNameValidator(nameRe: string []): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = (control.value);
    if (!nameRe.includes(forbidden)) {
      return {'forbiddenName': {value: control.value}};
    } else {
      return null;
    }
  };
}

@Component({
  selector: 'app-project-buttons',
  templateUrl: './project-buttons.component.html',
  styleUrls: ['./project-buttons.component.styl']
})

export class ProjectButtonsComponent implements OnInit {


  private subscriptions: Subscription[] = [];
  public tasks: Task[];
  public projects: Project [];
  priorities: string[] = ['BLOCKER', 'CRITICAL', 'MAJOR', 'NORMAL', "MINOR"];
  projectCodeControl : FormControl = new FormControl();
  priorityControl : FormControl = new FormControl('', forbiddenNameValidator(this.priorities));
  public editableProject: Project = new Project;
  public editableTask: Task = new Task();
  public min = new Date();
  public max = new Date();
  public taskAmount: number;
  public tasksOnPage: number;
  public currentPage: number;
  public modalRef: BsModalRef;
  constructor(private taskService: TaskService, private projectService: ProjectService,
              private statusService: StatusService, private modalService: BsModalService, private data: TaskSharingService) {
  }

  ngOnInit() {
    this.data.currentTaskAmount.subscribe(taskAmount => this.taskAmount = taskAmount);
  }

  public addTask(): void {
    this.data.currentPage.subscribe(currentPage => {
      this.currentPage = currentPage
    });
    this.data.currentTasksOnPage.subscribe(currentTasksOnPage => {
      this.tasksOnPage = currentTasksOnPage;
    });
    this.editableTask.projectCode = this.projectCodeControl.value;
    this.editableTask.priority = this.priorityControl.value;
    this.subscriptions.push(this.taskService.addTask(this.editableTask).subscribe(() => {
      this.data.updateTaskAmount();
      this.closeModalWindow();
    }));
  }
  public addProject(): void {
    this.subscriptions.push(this.projectService.addProject(this.editableProject).subscribe(() => {
      this.projectService.getProjects();
      this.closeModalWindow();
    }));
  }
  public closeModalWindow(): void {
    this.projectCodeControl.reset();
    this.priorityControl.reset();
    this.modalRef.hide();
  }

  public observeProjectChanges(formControl: FormControl) {
    formControl.valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe(
      projectCode => {
        if (projectCode!='') {
          this.projectService.getAutocompleteProjects(projectCode).subscribe(
            projects => {
              this.projects = projects as Project [];
            }
          )
        } else {
          this.loadProjects();
        }
      }
    );
  }
  public openModalWindow(template: TemplateRef<any>): void {
    this.max.setFullYear(this.min.getFullYear()+1);
    this.loadProjects();
    this.observeProjectChanges(this.projectCodeControl);
    this.refreshProjects();
    this.refreshTask();
    this.modalRef = this.modalService.show(template);
  }

  public openModalWindowProject(template: TemplateRef<any>): void {
    this.refreshProject();
    this.modalRef = this.modalService.show(template);
  }
  private loadProjects(): void {
    this.subscriptions.push(this.projectService.getProjects().subscribe(projects => {
      this.projects = projects as Project[];
      this.projectCodeControl.setValidators([forbiddenNameValidator(projects.map(projectCodes => projectCodes.projectCode))]);
    }));
  };

  private refreshTask(): void {
    this.editableTask = new Task();
  }
  private refreshProject(): void {
    this.editableProject = new Project();
  }
  private refreshProjects(): void {
    this.projects = [];
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


}
