import {Component, OnInit} from '@angular/core';
import {ProjectButtonsComponent} from "../project-buttons.component";

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.styl']
})
export class NewProjectComponent extends ProjectButtonsComponent implements OnInit {

  ngOnInit() {
  }


}
