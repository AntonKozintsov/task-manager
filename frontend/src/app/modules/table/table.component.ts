import {Component, OnInit} from '@angular/core';
import {Task} from "../models/task";
import {Subscription} from "rxjs";
import {TaskService} from "../../services/task.service";
import {ProjectService} from "../../services/project.service";
import {TaskSharingService} from "../../services/task-sharing.service";
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {Project} from "../models/project";
import {TokenStorageService} from "../../services/toke-storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.styl']
})
export class TableComponent implements OnInit {

  public tasks: Task [];
  public subscriptions: Subscription[] = [];
  public currentPage: number;
  public currentTasksOnPage: number = 5;
  taskIdentityControl: FormControl = new FormControl();
  public totalPagesArr: number [];
  public userName: string;

  constructor(public taskService: TaskService, public data: TaskSharingService, public tokenStorage: TokenStorageService,
              public router: Router) {

  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.userName = this.tokenStorage.getUsername();
      this.data.currentTasksOnPage.subscribe(currentTasksOnPage => {
        this.currentTasksOnPage = currentTasksOnPage;
      });
      this.data.currentPage.subscribe(currentPage => {
        this.currentPage = currentPage;
        this.loadTasks();
      });
      this.data.currentTaskAmount.subscribe(() => {
        this.loadTasks();
      });
      // public tasksOnPage: string [] = ['5','8','10'];
      this.taskService.updateTasks().subscribe(tasks => {
        this.tasks = tasks as Task [];
        if (tasks.length != 0) {
          this.currentTasksOnPage = tasks.length;
          console.log(this.totalPagesArr);
          this.totalPagesArr = [0];
        }
      });
      this.data.updateTaskAmount();
    } else {
      this.router.navigate(['error']);
    }
  }

  private loadTasks(): void {
    this.subscriptions.push(this.taskService.getPageableTasks(this.currentPage, this.currentTasksOnPage).subscribe(tasks => {
      this.tasks = tasks.taskModelList as Task[];
      console.log(this.currentTasksOnPage);
      console.log(this.currentPage);
    }));
  }

  public searchTasks(taskIdentity: string) {
    this.taskService.searchTasks(taskIdentity);
  };

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

}
