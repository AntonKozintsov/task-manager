import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {TaskService} from "./task.service";
import {Task} from "../modules/models/task";

@Injectable({
  providedIn: 'root'
})
export class TaskSharingService {


  private page = new BehaviorSubject(0);
  currentPage = this.page.asObservable();

  private taskAmount = new BehaviorSubject(0);
  currentTaskAmount = this.taskAmount.asObservable();

  private tasksOnPage = new BehaviorSubject(5);
  currentTasksOnPage = this.tasksOnPage.asObservable();

  constructor(private taskService: TaskService) { }

  ngOnInit() {
  }


  // updateTaskAmount(taskAmount: number) {
  //   // this.taskService.getPageableTasks(0,1 ).subscribe(pageableTasks => {
  //   //   this.taskAmount.next(pageableTasks.totalTasks);
  //   // });
  //   this.taskAmount.next(taskAmount);
  // }
  updateTaskAmount() {
    this.taskService.getPageableTasks(0,1 ).subscribe(pageableTasks => {
      this.taskAmount.next(pageableTasks.totalTasks);
    });
    // this.taskAmount.next(taskAmount);
  }
  updateCurrentPage(pageNumber: number) {
    this.page.next(pageNumber);
  }
  updateCurrentTasksOnPage(tasksOnPage: number) {
    this.tasksOnPage.next(tasksOnPage);
  }
}
