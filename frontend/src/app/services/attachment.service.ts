import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest, HttpResponse} from "@angular/common/http";
import {Attachment} from "../modules/models/attachment";
import {Observable} from "rxjs";
import {Task} from "../modules/models/task";
import {DomSanitizer} from "@angular/platform-browser";
import {AttachmentModel} from "../modules/models/attachment-model";

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {

  constructor(private httpClient: HttpClient) {
  }

  loadFile(formData: FormData): Observable<HttpEvent<{}>> {

    const req = new HttpRequest('POST', 'api/tasks/attachments', formData, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.httpClient.request('POST', 'api/tasks/attachments', req );
  }
  getImage(filename: string): Observable<AttachmentModel> {
    return this.httpClient.get<AttachmentModel>('api/tasks/attachments/' + filename);
  }
}
