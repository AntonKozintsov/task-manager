import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Project} from "../modules/models/project";
import {Task} from "../modules/models/task";
import {debounceTime} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) {
  }


  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>('/api/projects');
  }

  addProject(project: Project): Observable<Project> {
    return this.http.post<Project>('/api/projects', project);
  }
  getAutocompleteProjects(input: string): Observable<Project[]> {
    return this.http.get<Project[]>('/api/projects/' + input).pipe(debounceTime(500));
  }
}
