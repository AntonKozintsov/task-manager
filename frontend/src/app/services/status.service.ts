import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Project} from "../modules/models/project";
import {Status} from "../modules/models/status";

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  constructor(private http: HttpClient) {
  }

  getStatuses(): Observable<Status[]> {
    return this.http.get<Status[]>('/api/statuses');
  }
}
