import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable, ReplaySubject, Subject} from "rxjs";
import {Task} from "../modules/models/task";
import {UserStatus} from "../modules/models/userStatus";
import {PageableTask} from "../modules/models/pageableTask";
import {TokenStorageService} from "./toke-storage.service";
import {Project} from "../modules/models/project";
import {debounceTime} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private tasks$: Subject<Task []>;

  constructor(private http: HttpClient, private token: TokenStorageService) {
  }

  updateTasks(): Observable<Task[]> {
    if (!this.tasks$) {
      this.tasks$ = new ReplaySubject<Task []>(1);
      this.tasks$.next([]);
    }
    return this.tasks$;
  }

  getPageableTasks(pagenumber: number, tasksOnPage: number): Observable<PageableTask> {
    return this.http.get<PageableTask>('/api/tasks/pageable/' + pagenumber + "/" + tasksOnPage);
  }

  getPageable(pagenumber: number, tasksOnPage:number) : void {
    this.http.get<PageableTask>('/api/tasks/pageable/' + pagenumber + "/" + tasksOnPage).subscribe(pageableTask => {
      this.tasks$.next(pageableTask.taskModelList);
    });
  }

  getTasks(): void {
    this.http.get<Task[]>('/api/tasks').subscribe((tasks) => {
      this.tasks$.next(tasks);
    });
  }

  addTask(task: Task): Observable<Task> {
    return this.http.post<Task>('/api/tasks/save', task);
  }

  getTaskById(id: string): Observable<Task> {
    return this.http.get<Task>('/api/tasks/' + id);
  }

  updateStatus(status: UserStatus): Observable<Task> {
    return this.http.post<Task>('/api/tasks', status);
  }
  // searchTasks(taskIdentity: string): Observable<Task[]> {
  //   return this.http.get<Task[]>('/api/tasks/search/' + taskIdentity).pipe(debounceTime(500));
  // }
  searchTasks(taskIdentity: string): void {
    this.http.get<Task[]>('/api/tasks/search/' + taskIdentity).subscribe((tasks) => {
      this.tasks$.next(tasks);
    });
  }
}
