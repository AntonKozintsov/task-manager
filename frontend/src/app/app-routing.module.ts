import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProjectViewComponent} from "./modules/project-view/project-view.component";
import {TaskViewComponent} from "./modules/task-view/task-view.component";
import {LoginComponent} from "./modules/login/login.component";
import {ErrorComponent} from "./modules/error/error.component";

const routes: Routes = [
  {path: 'tasks', component: ProjectViewComponent },
  {path: 'tasks/:userId', component: TaskViewComponent },
  {path: 'login', component: LoginComponent },
  {path: 'error', component: ErrorComponent }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
