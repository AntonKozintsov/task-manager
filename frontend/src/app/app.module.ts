import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ModalModule } from "ngx-bootstrap/modal";
import { FormsModule } from "@angular/forms";
import {OWL_DATE_TIME_FORMATS, OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {PaginationComponent} from "./modules/pagination/pagination.component";
import {TableComponent} from "./modules/table/table.component";
import {ProjectButtonsComponent} from "./modules/project-buttons/project-buttons.component";
import {NewProjectComponent} from "./modules/project-buttons/new-project/new-project.component";
import {NewUserComponent} from "./modules/project-buttons/new-user/new-user.component";
import {MY_NATIVE_FORMATS, NewTaskComponent} from "./modules/project-buttons/new-task/new-task.component";
import { ProjectNavbarComponent } from './modules/project-navbar/project-navbar.component';
import { ProjectViewComponent } from './modules/project-view/project-view.component';
import { TaskViewComponent } from './modules/task-view/task-view.component';
import { TaskNavbarComponent } from './modules/task-navbar/task-navbar.component';
import {MatAutocompleteModule, MatDialog, MatDialogModule, MatInputModule} from "@angular/material";
import {AuthInterceptor, httpInterceptorProviders} from "./services/auth-interceptor";
import { LoginComponent } from './modules/login/login.component';
import { ErrorComponent } from './modules/error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    PaginationComponent,
    ProjectButtonsComponent,
    NewProjectComponent,
    NewTaskComponent,
    NewUserComponent,
    ProjectNavbarComponent,
    ProjectViewComponent,
    TaskViewComponent,
    TaskNavbarComponent,
    LoginComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
