package com.anton.backend.model;

import lombok.Data;

@Data
public class UserStatus {

    private Long userId;
    private String status;
}
