package com.anton.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class UserModel {

    private Long id;
    private String name;
    private String username;
    private String email;
    private String password;
    private List<String> roles;
}
