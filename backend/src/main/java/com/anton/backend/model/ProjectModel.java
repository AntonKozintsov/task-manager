package com.anton.backend.model;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProjectModel {
    private String projectCode;
    private String summary;
}
