package com.anton.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskModel {
    private Long id;
    private String priority;
    private String status;
    private String taskIdentity;
    private String description;
    private String projectCode;
    private String created;
    private String updated;
    private String resolved;
    private String closed;
    private String dueDate;
    private String estimation;
    private String assignee;
    private String reporter;
    private List<String> attachments;

}
