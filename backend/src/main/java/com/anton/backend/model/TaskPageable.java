package com.anton.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class TaskPageable {
    private List<TaskModel> taskModelList;
    private Long totalTasks;
}
