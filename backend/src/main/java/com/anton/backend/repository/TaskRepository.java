package com.anton.backend.repository;

import com.anton.backend.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    Task findTaskById(Long id);

    Task findTaskByTaskIdentity(String taskIdentity);

    List<Task> findTasksByTaskIdentityStartingWith(String taskIdentity);
}
