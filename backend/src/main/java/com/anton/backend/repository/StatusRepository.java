package com.anton.backend.repository;

import com.anton.backend.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<Status, Long> {

    Status findStatusByStatus(String status);
}
