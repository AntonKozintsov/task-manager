package com.anton.backend.repository;

import com.anton.backend.entity.Priority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriorityRepository extends JpaRepository<Priority, Long> {

    Priority findPriorityByPriority(String priority);
}
