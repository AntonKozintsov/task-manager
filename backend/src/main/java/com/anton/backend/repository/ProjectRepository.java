package com.anton.backend.repository;

import com.anton.backend.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    List<Project> findProjectsByProjectCodeStartingWith(String projectCode);

    Project findProjectByProjectCode(String projectCode);
}
