package com.anton.backend.repository;

import com.anton.backend.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

    List<Attachment> findAttachmentsByTaskId(Long taskId);
}
