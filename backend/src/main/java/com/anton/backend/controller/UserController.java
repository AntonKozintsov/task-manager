package com.anton.backend.controller;


import com.anton.backend.model.UserModel;
import com.anton.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/login/{username}")
    public ResponseEntity<UserModel> findUserByUsername(@PathVariable(name = "username") String username) {
        UserModel user = userService.findByUserName(username);
        return ResponseEntity.ok(user);
    }

    @GetMapping("")
    public List<UserModel> getAllUsers() {
        return userService.findAll();
    }

    @PostMapping()
    public UserModel saveUser(@RequestBody UserModel user) {
        return userService.save(user);
    }
}
