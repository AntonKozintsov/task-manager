package com.anton.backend.controller;

import com.anton.backend.model.ProjectModel;
import com.anton.backend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("")
    public List<ProjectModel> getAllProjects() {
        return projectService.findAll();
    }

    @GetMapping("/{projectCode}")
    public List<ProjectModel> getSearchedProjects(@PathVariable String projectCode) {
        return projectService.findSearched(projectCode);
    }


    @PostMapping("")
    public ProjectModel save(@RequestBody ProjectModel projectModel) {
        return this.projectService.save(projectModel);
    }
}
