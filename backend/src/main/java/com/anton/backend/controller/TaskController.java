package com.anton.backend.controller;

import com.anton.backend.model.TaskModel;
import com.anton.backend.model.TaskPageable;
import com.anton.backend.model.UserStatus;
import com.anton.backend.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }


    @GetMapping("")
    public List<TaskModel> getAllTasks() {
        return taskService.findAll();
    }

    @GetMapping("/pageable/{pageNumber}/{currentTasksOnPage}")
    public TaskPageable getPageableTasks(@PathVariable Integer pageNumber, @PathVariable Integer currentTasksOnPage) {
        Pageable pageable = PageRequest.of(pageNumber, currentTasksOnPage);
        return taskService.findAll(pageable);
    }

    @GetMapping("/search/{taskIdentity}")
    public List<TaskModel> getSearchedProjects(@PathVariable String taskIdentity) {
        return taskService.findSearched(taskIdentity);
    }

    @PostMapping("/addTask")
    public TaskModel save(@RequestBody TaskModel taskModel) {
        return taskService.save(taskModel);
    }

    @GetMapping("/{id}")
    public TaskModel getTaskById(@PathVariable Long id) {
        System.out.println(taskService.findTaskById(id).getClosed());
        return taskService.findTaskById(id);
    }

    @PostMapping("")
    public TaskModel updateStatus(@RequestBody UserStatus status) {
        System.out.println(status);
        return taskService.updateStatus(status);
    }

    @PostMapping("/assign")
    public TaskModel toAssign(@RequestBody TaskModel taskModel) {
        return taskService.save(taskModel);
    }

}
