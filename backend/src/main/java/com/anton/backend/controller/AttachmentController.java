package com.anton.backend.controller;

import com.anton.backend.model.AttachmentModel;
import com.anton.backend.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/tasks/attachments")

public class AttachmentController {

    private final AttachmentService attachmentService;

    @Autowired
    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @PostMapping("")
    public String uploadAttachment(@RequestParam Long taskId, @RequestParam(value = "file", required = false) MultipartFile file) {
        attachmentService.save(taskId, file);
        return HttpStatus.OK.toString();
    }

    @GetMapping(value = "/{filename}")
    public AttachmentModel getFileFromFileSystem(@PathVariable String filename) throws IOException {
        return new AttachmentModel(attachmentService.downloadFile(filename));
    }
}
