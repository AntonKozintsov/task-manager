package com.anton.backend.service.impl;

import com.anton.backend.converter.TaskConverter;
import com.anton.backend.entity.Project;
import com.anton.backend.entity.Task;
import com.anton.backend.model.TaskModel;
import com.anton.backend.model.TaskPageable;
import com.anton.backend.model.UserStatus;
import com.anton.backend.repository.TaskRepository;
import com.anton.backend.service.ProjectService;
import com.anton.backend.service.StatusService;
import com.anton.backend.service.TaskService;
import com.anton.backend.service.UserService;
import com.anton.backend.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    private final ProjectService projectService;

    private final UserService userService;

    private final TaskConverter taskConverter;

    private final DateUtil dateUtil;


    @Autowired
    private StatusService statusService;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, TaskConverter taskConverter, DateUtil dateUtil, ProjectService projectService, UserService userService) {
        this.taskRepository = taskRepository;
        this.taskConverter = taskConverter;
        this.dateUtil = dateUtil;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Transactional
    @Override
    public List<TaskModel> findAll() {
        return taskConverter.toModel(taskRepository.findAll());
    }

    @Transactional
    @Override
    public TaskPageable findAll(Pageable pageable) {
        Page<Task> page = taskRepository.findAll(pageable);
        return new TaskPageable(taskConverter.toModel(taskRepository.findAll(pageable).stream().collect(Collectors.toList())),
                page.getTotalElements());
    }

    @Transactional
    @Override
    public TaskModel save(TaskModel taskModel) {
        Project project = projectService.findByProjectCode(taskModel.getProjectCode());
        taskModel.setTaskIdentity(taskModel.getTaskIdentity());
        Task task = taskRepository.save(taskConverter.toEntity(taskModel));
        task.setTaskIdentity(task.getTaskIdentity() + "-" + task.getProjectCode() + "-" + task.getId());
        List<Task> taskList = project.getTaskList();
        taskList.add(task);
        projectService.save(project);
        return taskConverter.toModel(task);
    }

    @Transactional
    @Override
    public TaskModel findTaskById(Long id) {

        return taskConverter.toModel(taskRepository.findTaskById(id));
    }

    @Transactional
    @Override
    public TaskModel updateStatus(UserStatus status) {
        Task task = taskRepository.findTaskById(status.getUserId());
        task.setStatus(statusService.findStatusByStatus((status.getStatus())));
        task.setUpdatedDate(dateUtil.stringToDate(dateUtil.dateToString(new Date())));
        if (task.getStatus().getStatus().equals("CLOSED")) {
            task.setClosedDate(dateUtil.stringToDate(dateUtil.dateToString(new Date())));
        } else if (task.getStatus().getStatus().equals("RESOLVED")) {
            task.setResolvedDate(dateUtil.stringToDate(dateUtil.dateToString(new Date())));
        }
        TaskModel taskModel = taskConverter.toModel(taskRepository.save(task));
        System.out.println(taskModel);
        return taskModel;
    }

    @Transactional
    @Override
    public List<TaskModel> findSearched(String taskIdentity) {
        return taskConverter.toModel(taskRepository.findTasksByTaskIdentityStartingWith(taskIdentity));
    }
}
