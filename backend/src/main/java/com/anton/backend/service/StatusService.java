package com.anton.backend.service;

import com.anton.backend.entity.Status;
import com.anton.backend.model.StatusModel;

import java.util.List;

public interface StatusService {

    List<StatusModel> findAll();

    Status findStatusByStatus(String status);

}
