package com.anton.backend.service;

import com.anton.backend.entity.User;
import com.anton.backend.model.UserModel;

import java.util.List;

public interface UserService {


    List<UserModel> findAll();

    UserModel findByUserName(String name);

    User findUserByUsername(String name);

    UserModel save(UserModel user);

    void delete(Long id);
}
