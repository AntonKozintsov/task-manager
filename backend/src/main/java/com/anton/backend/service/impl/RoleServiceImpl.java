package com.anton.backend.service.impl;

import com.anton.backend.entity.Role;
import com.anton.backend.repository.RoleRepository;
import com.anton.backend.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;


    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }


    @Transactional
    @Override
    public List<Role> findAll() {
        return repository.findAll();
    }

    @Override
    public Role findByName(String roleName) {
        return repository.findByName(roleName);
    }
}
