package com.anton.backend.service;

import com.anton.backend.entity.Attachment;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface AttachmentService {

    void save(Long taskId, MultipartFile file);

    List<Attachment> findAllAttachments(Long taskId);

    String downloadFile(String fileName) throws IOException;
}
