package com.anton.backend.service;

import com.anton.backend.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();

    Role findByName(String roleName);
}
