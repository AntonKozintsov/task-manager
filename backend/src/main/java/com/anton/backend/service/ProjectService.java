package com.anton.backend.service;

import com.anton.backend.entity.Project;
import com.anton.backend.model.ProjectModel;

import java.util.List;

public interface ProjectService {


    List<ProjectModel> findAll();

    Project findByProjectCode(String projectCode);

    List<ProjectModel> findSearched(String projectCode);

    ProjectModel save(ProjectModel projectModel);

    Project save(Project project);

    void delete(Long id);
}
