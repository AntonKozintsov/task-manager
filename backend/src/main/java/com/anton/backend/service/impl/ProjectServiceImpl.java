package com.anton.backend.service.impl;

import com.anton.backend.converter.ProjectConverter;
import com.anton.backend.entity.Project;
import com.anton.backend.model.ProjectModel;
import com.anton.backend.repository.ProjectRepository;
import com.anton.backend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository repository;

    private final ProjectConverter projectConverter;

    @Autowired
    public ProjectServiceImpl(ProjectRepository repository, ProjectConverter projectConverter) {
        this.repository = repository;
        this.projectConverter = projectConverter;
    }

    @Transactional
    @Override
    public ProjectModel save(ProjectModel projectModel) {
        return projectConverter.toModel(repository.save(projectConverter.toEntity(projectModel)));
    }

    @Transactional
    @Override
    public Project save(Project project) {
        return repository.save(project);
    }

    @Transactional
    @Override
    public List<ProjectModel> findAll() {
        return projectConverter.toModel(repository.findAll());
    }

    @Override
    public Project findByProjectCode(String projectCode) {
        return repository.findProjectByProjectCode(projectCode);
    }

    @Transactional
    @Override
    public List<ProjectModel> findSearched(String projectCode) {
        return projectConverter.toModel(repository.findProjectsByProjectCodeStartingWith(projectCode));
    }

    @Transactional
    @Override
    public void delete(Long id) {
        repository.deleteById(id);

    }
}
