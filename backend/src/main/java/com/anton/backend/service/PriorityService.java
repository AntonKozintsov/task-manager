package com.anton.backend.service;

import com.anton.backend.entity.Priority;

import java.util.List;

public interface PriorityService {

    List<Priority> findAll();

    Priority findPriorityByPriority(String priority);
}
