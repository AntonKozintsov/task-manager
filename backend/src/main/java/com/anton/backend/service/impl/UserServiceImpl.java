package com.anton.backend.service.impl;

import com.anton.backend.converter.UserConverter;
import com.anton.backend.entity.User;
import com.anton.backend.model.UserModel;
import com.anton.backend.repository.UserRepository;
import com.anton.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    private final UserConverter userConverter;

    @Autowired
    public UserServiceImpl(UserRepository repository, UserConverter userConverter) {
        this.repository = repository;
        this.userConverter = userConverter;
    }

    @Transactional

    @Override
    public List<UserModel> findAll() {
        return userConverter.toModel(repository.findAll());
    }

    @Transactional
    @Override
    public UserModel findByUserName(String name) {
        return userConverter.toModel(repository.findByUsername(name));
    }

    @Override
    public User findUserByUsername(String name) {
        return repository.findByUsername(name);
    }

    @Transactional
    @Override
    public UserModel save(UserModel user) {
        return userConverter.toModel(repository.save(userConverter.toEntity(user)));
    }


    @Transactional
    @Override
    public void delete(Long id) {
        repository.deleteById(id);

    }
}
