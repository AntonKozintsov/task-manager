package com.anton.backend.service.impl;

import com.anton.backend.entity.Attachment;
import com.anton.backend.entity.Task;
import com.anton.backend.repository.AttachmentRepository;
import com.anton.backend.repository.TaskRepository;
import com.anton.backend.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final TaskRepository taskRepository;
    @Autowired
    private HttpServletRequest request;

    @Autowired
    public AttachmentServiceImpl(AttachmentRepository attachmentRepository, TaskRepository taskRepository) {
        this.attachmentRepository = attachmentRepository;
        this.taskRepository = taskRepository;
    }


    @Override
    public void save(Long taskId, MultipartFile file) {
        Attachment attachment = new Attachment();
        List<String> attachments = attachmentRepository.findAttachmentsByTaskId(taskId).stream().map(Attachment::getPath).collect(Collectors.toList());
        if (!attachments.contains(file.getOriginalFilename())) {
            try {
                Path filePath = Paths.get("upload-data", file.getOriginalFilename());
                OutputStream os = Files.newOutputStream(filePath);
                os.write(file.getBytes());
//                Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
                Task task = taskRepository.findTaskById(taskId);
                attachment.setPath(file.getOriginalFilename());
                attachment.setTask(task);
                attachmentRepository.save(attachment);
                taskRepository.save(task);
            } catch (IOException e) {
                throw new RuntimeException("File error");
            }
        }
    }

    @Override
    public List<Attachment> findAllAttachments(Long taskId) {
        return attachmentRepository.findAttachmentsByTaskId(taskId);
    }

    @Override
    public String downloadFile(String fileName) throws IOException {
        File file = new File("upload-data/" + fileName);
        return Base64.getEncoder().withoutPadding().encodeToString(Files.readAllBytes(file.toPath()));
    }
}
