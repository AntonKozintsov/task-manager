package com.anton.backend.service;

import com.anton.backend.model.TaskModel;
import com.anton.backend.model.TaskPageable;
import com.anton.backend.model.UserStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TaskService {


    List<TaskModel> findAll();

    TaskPageable findAll(Pageable pageable);

    TaskModel save(TaskModel taskModel);

    TaskModel findTaskById(Long id);

    TaskModel updateStatus(UserStatus status);

    @Transactional
    List<TaskModel> findSearched(String taskIdentity);

//    List<Attachment> findAllAttachments(Long taskId);

}
