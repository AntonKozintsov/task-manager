package com.anton.backend.service.impl;

import com.anton.backend.converter.StatusConverter;
import com.anton.backend.entity.Status;
import com.anton.backend.model.StatusModel;
import com.anton.backend.repository.StatusRepository;
import com.anton.backend.service.StatusService;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@ToString
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    private final StatusConverter statusConverter;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository, StatusConverter statusConverter) {
        this.statusRepository = statusRepository;
        this.statusConverter = statusConverter;
    }

    @Transactional
    @Override
    public List<StatusModel> findAll() {
        return statusConverter.toModel(statusRepository.findAll());
    }

    @Override
    public Status findStatusByStatus(String status) {
        return statusRepository.findStatusByStatus(status);
    }


}
