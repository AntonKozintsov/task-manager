package com.anton.backend.service.impl;

import com.anton.backend.entity.Priority;
import com.anton.backend.repository.PriorityRepository;
import com.anton.backend.service.PriorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PriorityServiceImpl implements PriorityService {

    private final PriorityRepository repository;

    @Autowired
    public PriorityServiceImpl(PriorityRepository repository) {
        this.repository = repository;
    }

    @Transactional
    @Override
    public List<Priority> findAll() {
        return (List<Priority>) repository.findAll();
    }

    @Override
    public Priority findPriorityByPriority(String priority) {
        return repository.findPriorityByPriority(priority);
    }
}
