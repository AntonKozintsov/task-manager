package com.anton.backend.converter;

import com.anton.backend.entity.*;
import com.anton.backend.model.TaskModel;
import com.anton.backend.service.AttachmentService;
import com.anton.backend.service.PriorityService;
import com.anton.backend.service.StatusService;
import com.anton.backend.service.UserService;
import com.anton.backend.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TaskConverter {

    private final PriorityService priorityService;

    private final StatusService statusService;

    private final UserService userService;

    private final DateUtil dateUtil;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    public TaskConverter(PriorityService priorityService, StatusService statusService, UserService userService, DateUtil dateUtil) {
        this.priorityService = priorityService;
        this.statusService = statusService;
        this.userService = userService;
        this.dateUtil = dateUtil;
    }

    public TaskModel toModel(Task task) {
        String created = dateUtil.dateToString(task.getCreatedDate());
        String updated = dateUtil.dateToString(task.getUpdatedDate());
        String resolved = dateUtil.dateToString(task.getResolvedDate());
        String closed = dateUtil.dateToString(task.getClosedDate());
        String dueDate = dateUtil.dateToString(task.getDueDate());
        String estimation = dateUtil.dateToString(task.getEstimationDate());
        String assignee;
        List<String> attachments = attachmentService.findAllAttachments(task.getId()).stream().map(Attachment::getPath).collect(Collectors.toList());
        if (task.getAssignee() == null) {
            assignee = "No assignee yet";
        } else {
            assignee = (task.getAssignee().getName());
        }
        String reporter;
        if (task.getReporter() == null) {
            reporter = "";
        } else {
            reporter = task.getReporter().getName();
        }
        TaskModel taskModel = new TaskModel();
        taskModel.setId(task.getId());
        taskModel.setPriority(task.getPriority().getPriority());
        taskModel.setStatus(task.getStatus().getStatus());
        taskModel.setTaskIdentity(task.getTaskIdentity());
        taskModel.setDescription(task.getDescription());
        taskModel.setProjectCode(task.getProjectCode());
        taskModel.setCreated(created);
        taskModel.setUpdated(updated);
        taskModel.setResolved(resolved);
        taskModel.setDueDate(dueDate);
        taskModel.setEstimation(estimation);
        taskModel.setClosed(closed);
        taskModel.setAssignee(assignee);
        taskModel.setReporter(reporter);
        taskModel.setAttachments(attachments);
//        return new TaskModel(task.getId(), task.getPriority().getPriority(), task.getStatus().getStatus(), task.getTaskIdentity(),
//                task.getDescription(), task.getProjectCode(), created, updated, resolved, closed, dueDate,
//                estimation, assignee, reporter, attachments);
        return taskModel;
    }

    public Task toEntity(TaskModel taskModel) {
        Priority priority = priorityService.findPriorityByPriority(taskModel.getPriority().toUpperCase());
        Status status = statusService.findStatusByStatus(taskModel.getStatus());
        User assignee = null;
        if (taskModel.getAssignee() != null) {
            assignee = userService.findUserByUsername(taskModel.getAssignee());
        }
        User reporter = null;
        if (taskModel.getReporter() != null) {
            reporter = userService.findUserByUsername(taskModel.getReporter());
        }
        Date created = dateUtil.stringToDate(taskModel.getCreated());
        Date updated = dateUtil.stringToDate(taskModel.getUpdated());
        Date resolved = dateUtil.stringToDate(taskModel.getResolved());
        Date closed = dateUtil.stringToDate(taskModel.getClosed());
        Date dueDate = dateUtil.stringToDate(taskModel.getDueDate());
        Date estimation = dateUtil.stringToDate(taskModel.getEstimation());
        List<Attachment> attachments = attachmentService.findAllAttachments(taskModel.getId());
        return new Task(taskModel.getId(), priority, status, taskModel.getTaskIdentity(), taskModel.getDescription(), taskModel.getProjectCode(),
                created, updated, resolved, closed, dueDate, estimation, assignee, reporter, attachments);
    }

    public List<TaskModel> toModel(List<Task> taskList) {
        return taskList.stream().map(this::toModel)
                .collect(Collectors.toList());
    }

}
