package com.anton.backend.converter;

import com.anton.backend.entity.Project;
import com.anton.backend.model.ProjectModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProjectConverter {

    public ProjectModel toModel(Project project) {
        return new ProjectModel(project.getProjectCode(),
                project.getSummary()
        );
    }

    public Project toEntity(ProjectModel projectModel) {
        return new Project(projectModel.getProjectCode(), projectModel.getSummary()
        );
    }

    public List<ProjectModel> toModel(List<Project> projectList) {
        return projectList.stream().map(this::toModel)
                .collect(Collectors.toList());
    }
}
