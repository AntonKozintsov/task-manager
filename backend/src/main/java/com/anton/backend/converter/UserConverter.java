package com.anton.backend.converter;

import com.anton.backend.entity.Role;
import com.anton.backend.entity.User;
import com.anton.backend.model.UserModel;
import com.anton.backend.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserConverter {

    private final RoleService roleService;

    @Autowired
    public UserConverter(RoleService roleService) {
        this.roleService = roleService;
    }

    public UserModel toModel(User user) {
        List<Role> roleList = new ArrayList<>(user.getRoles());
        List<String> roles = new ArrayList<>();
        for (Role role : roleList) {
            roles.add(role.getName().toString());
        }
        return new UserModel(user.getId(), user.getName(), user.getUsername(), user.getEmail(), user.getPassword(), roles);
    }

    public User toEntity(UserModel userModel) {
        Set<Role> roleSet = new HashSet<>();
        for (int i = 0; i < userModel.getRoles().size(); i++) {
            Role role = roleService.findByName(userModel.getRoles().get(i));
            roleSet.add(role);
        }
        return new User(userModel.getName(), userModel.getUsername(), userModel.getEmail(), userModel.getPassword(), roleSet);
    }

    public List<UserModel> toModel(List<User> userList) {
        return userList.stream().map(this::toModel)
                .collect(Collectors.toList());
    }

}
