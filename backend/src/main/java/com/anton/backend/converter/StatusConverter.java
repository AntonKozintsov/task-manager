package com.anton.backend.converter;

import com.anton.backend.entity.Status;
import com.anton.backend.model.StatusModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class StatusConverter {

    public StatusModel toModel(Status status) {
        return new StatusModel(status.getStatus());
    }

    public Status toEntity(StatusModel statusModel) {
        return new Status(statusModel.getStatus());
    }

    public List<StatusModel> toModel(List<Status> statusList) {
        return statusList.stream().map(this::toModel)
                .collect(Collectors.toList());
    }
}
