package com.anton.backend.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(targetEntity = Priority.class)
    private Priority priority;

    @OneToOne(targetEntity = Status.class)
    private Status status;

    private String taskIdentity;

    private String description;

    private String projectCode;

    private Date createdDate;

    private Date updatedDate;

    private Date resolvedDate;

    private Date closedDate;

    private Date dueDate;

    private Date estimationDate;

    @OneToOne(targetEntity = User.class)
    private User assignee;

    @OneToOne(targetEntity = User.class)
    private User reporter;

    @OneToMany(targetEntity = Comments.class)
    private List<Comments> comments;

    @OneToMany(targetEntity = Attachment.class)
    private List<Attachment> attachments;

    public Task(Long id, Priority priority, Status status, String taskIdentity, String description, String projectCode,
                Date createdDate, Date updatedDate, Date resolvedDate, Date closedDate,
                Date dueDate, Date estimationDate, User assignee, User reporter, List<Attachment> attachments) {
        this.id = id;
        this.priority = priority;
        this.status = status;
        this.taskIdentity = taskIdentity;
        this.description = description;
        this.projectCode = projectCode;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.resolvedDate = resolvedDate;
        this.closedDate = closedDate;
        this.dueDate = dueDate;
        this.estimationDate = estimationDate;
        this.assignee = assignee;
        this.reporter = reporter;
        this.attachments = attachments;
    }
}
