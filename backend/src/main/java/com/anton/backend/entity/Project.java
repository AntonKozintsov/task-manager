package com.anton.backend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String projectCode;
    private String summary;
    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, targetEntity = Task.class)
    private List<Task> taskList;

    public Project(String projectCode, String summary) {
        this.projectCode = projectCode;
        this.summary = summary;
    }
}
