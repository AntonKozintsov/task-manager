-- role initial data
-- INSERT into taskmanager.role (role) values ('projectmanager');
-- INSERT into taskmanager.role (role) values ('developer');
-- INSERT into taskmanager.role (role) values ('tester');

-- priority initial data
INSERT into taskmanager.priority (priority)
values ('BLOCKER');
INSERT into taskmanager.priority (priority)
values ('CRITICAL');
INSERT into taskmanager.priority (priority)
values ('MAJOR');
INSERT into taskmanager.priority (priority)
values ('NORMAL');
INSERT into taskmanager.priority (priority)
values ('MINOR');

-- userStatus.ts initial data
INSERT into taskmanager.status (status)
values ('OPENED');
INSERT into taskmanager.status (status)
values ('IN PROGRESS');
INSERT into taskmanager.status (status)
values ('READY FOR TEST');
INSERT into taskmanager.status (status)
values ('RESOLVED');
INSERT into taskmanager.status (status)
values ('CLOSED');



